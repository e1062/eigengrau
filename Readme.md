# Eigengrau
Initializeaza 8 Oscilatoare de diferite forme care vor fi folosite de fiecare modul pentru a sintetiza diferite secvente de sunete.


## Componente

### "Breath"
- Reprezinta seedul momentului si actioneaza ca seed de intrare pentru restul componentelor
- Sa permita input (senzori, potentiometre) pentru development
- Sa expuna atat durata unui step cat si oscilatia
- Durata unui step sa poata fi modulata [?]

### Module
- fiecare modul este o sursa de sunet
- permite input (oscilatia principala + intervalul unui step)
- expune sunetul pentru a fi mixat

Fiecare modul va extinde urmatoarele metode:
- `void step();`
- `void updateControl();`
- `int next();`

- `step()` va updata valorile care se vor schimba la fiecare puls al orchestratorului. Va fi apelat in hook-ul principal `updateControl` dar doar la intervalul de timp la care se va trimite un nou puls.
- `updateControl()` va fi apelat la fiecare ciclu de update apelat de mozzi.

### Mixer
- Mixer x canale
- permite inputuri (module)
- are sistem de send (pentru efect)
- un efect care poate fi interschimbabi;
- permite automatizare pe canale individuale (pe volum [pe pan?])
- `NUMBER_OF_CANNELS` e definit ca ENV in platformio.ini
### Orchestrator
- selecteaza module si le asigneaza pe trackuri
- ofera logica de intrare/iesire pentru module
- decide durata de utilizare (playtme) a unui modul si programeaza modulele urmatoare