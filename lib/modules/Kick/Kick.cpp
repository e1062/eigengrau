#include "Oscil.h"
#include "ADSR.h"

#include "Kick.h"

#include "tables/square_analogue512_int8.h"
#include "tables/sin2048_int8.h"

Oscil <SQUARE_ANALOGUE512_NUM_CELLS, AUDIO_RATE> mKickSqr(SQUARE_ANALOGUE512_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> mKickSin(SIN2048_DATA);

ADSR <CONTROL_RATE, CONTROL_RATE> mKickPitchEnvelope;

byte mKickPitch;

Kick::Kick()
{
  mKickSqr.setFreq(440);
  
}

int Kick::next()
{
  return mKickSqr.next() & mKickSin.next();
}

void Kick::step() 
{
  mKickPitchEnvelope.setADLevels(0, 64);
  mKickPitchEnvelope.setTimes(0, 0, 128, 256);
  mKickPitchEnvelope.noteOn();
}

void Kick::updateControl()
{
  mKickPitchEnvelope.update();
  mKickPitch = mKickPitchEnvelope.next();
  mKickSqr.setFreq(mKickPitch);
  mKickSin.setFreq(mKickPitch);
}