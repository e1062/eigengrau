#ifndef Kick_h
#define Kick_h

#include "Module.h"

class Kick: public Module
{
  private:
    
  public:
    Kick();
    void step();
    void updateControl();
    int next();
};

#endif