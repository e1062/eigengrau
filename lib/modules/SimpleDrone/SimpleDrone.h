#ifndef SimpleDrone_h
#define SimpleDrone_h

#include "Module.h"

class SimpleDrone: public Module
{
  private:
    
  public:
    SimpleDrone();
    void step();
    void updateControl();
    int next();
};

#endif