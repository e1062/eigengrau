#include "Oscil.h"
#include "LowPassFilter.h"
#include "mozzi_rand.h"

#include "SimpleDrone.h"

#include "tables/sin2048_int8.h"
#include "tables/cos2048_int8.h"

Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> mSDroneSin1(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> mSDroneSin2(SIN2048_DATA);
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> mSDroneSin3(SIN2048_DATA);
Oscil<COS2048_NUM_CELLS, CONTROL_RATE> mSDroneCos1(COS2048_DATA);

LowPassFilter mSDroneLPF;

byte pitch;
uint8_t resonance = 32;

SimpleDrone::SimpleDrone()
{
  mSDroneSin1.setFreq(120);
  mSDroneSin2.setFreq(240);
  mSDroneSin3.setFreq(480);
  mSDroneLPF.setCutoffFreqAndResonance(255, resonance);
  mSDroneCos1.setFreq(1.3f);
}

int SimpleDrone::next()
{
  return mSDroneLPF.next(
    mSDroneSin1.next() &
    mSDroneSin2.next() &
    mSDroneSin3.next()
  );
}

void SimpleDrone::step() 
{
  mSDroneCos1.setFreq((float)rand(255)/64);
}

void SimpleDrone::updateControl()
{
  byte cutoff_freq = 100 + mSDroneCos1.next()/2;
  mSDroneLPF.setCutoffFreqAndResonance(cutoff_freq, resonance);
}