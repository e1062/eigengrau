#ifndef Orchestrator_h
#define Orchestrator_h

#include "Track.h"

class Orchestrator
{
  Track *tracks[4];
  public:
    Orchestrator();
    void begin();
    void step();
    void getCurrentStepState();
    void debugShowCurrentStepStatus();
};

#endif