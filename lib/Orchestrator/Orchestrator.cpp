#include "Orchestrator.h"
#include "Track.h"
#include <mozzi_rand.h>

const int MAX_LIFE_DURATION = 10;

Orchestrator::Orchestrator()
{

}

void Orchestrator::begin()
{
  for(int i = 0; i < 4; i++) {
    int lifeDuration = rand(2, MAX_LIFE_DURATION);
    tracks[i] = new Track(lifeDuration);
  }
}

void Orchestrator::step()
{
  for(int i = 0; i < NUMBER_OF_CHANNELS; i++) {
    Track *currentTrack = tracks[i];
    
    if(currentTrack->remainingSteps == 0) {
      currentTrack->reinitialize(rand(2, MAX_LIFE_DURATION));
    }

    currentTrack->advance();
  }
}

void Orchestrator::getCurrentStepState()
{
    debugShowCurrentStepStatus();
    // Track *currentTrack = tracks[i];
    // int currentStep = currentTrack->currentStep;

    // if(currentStep == 0) {
    //   // Intro track
    // }

    // if(currentStep == currentTrack->totalSteps - 1) {
    //   // Outro track
    // }

}

void Orchestrator::debugShowCurrentStepStatus()
{

  Serial.println("=== Orchestrator step ===");
  for(int i = 0; i < NUMBER_OF_CHANNELS; i++) {
    Track *currentTrack = tracks[i];
    int currentStep = currentTrack->currentStep;
    Serial.print("Track [");
    Serial.print(i);
    Serial.print("]    ");

    Serial.print("Current step: ");
    Serial.print(currentStep);
    Serial.print("     ");

    Serial.print("Total steps: ");
    Serial.print(currentTrack->totalSteps);
    Serial.print("     ");

    Serial.print("Remaining steps: ");
    Serial.println(currentTrack->remainingSteps);
  }
  Serial.println("===");
}

