#ifndef Track_h
#define Track_h

class Track
{

  public:
    Track(int _totalSteps);
    int totalSteps;
    int remainingSteps;
    int currentStep;
    int advance();
    void reinitialize(int _totalSteps);
};

#endif