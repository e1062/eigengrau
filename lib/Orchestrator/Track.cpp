#include "Arduino.h"
#include "Track.h"

Track::Track(int _totalSteps)
{
  totalSteps = _totalSteps;

  remainingSteps = totalSteps;
  
  currentStep = 0;
}

int Track::advance() {

  if(remainingSteps > 0) {
    currentStep += 1;
    remainingSteps = totalSteps - currentStep;
  } else {
    currentStep = totalSteps;
    remainingSteps = 0;
  }

  return remainingSteps;
}

void Track::reinitialize(int _totalSteps) {
  totalSteps = _totalSteps;
  remainingSteps = totalSteps;
  currentStep = 0;
}