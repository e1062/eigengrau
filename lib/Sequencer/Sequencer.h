#ifndef Sequencer_h
#define Sequencer_h

#include "Arduino.h"

class Sequencer 
{
  public:
    Sequencer();
    int step();
};

#endif