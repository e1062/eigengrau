#ifndef Module_h
#define Module_h

class Module
{
  public:
    Module();
    void step();
    void updateControl();
    virtual int next();
};

#endif