#ifndef Channel_h
#define Channel_h

#include "Arduino.h"
#include "Module.h"

class Channel
{
  public:
    Channel(int);
    int getIndex();
    void setInput(Module module);
    Module getInput();
    void setGain(int gain);
    boolean connected = false;
  private:
    int _index;
    Module _input;
    int _gain;
};

#endif