#ifndef Mixer_h
#define Mixer_h

#include "Arduino.h"
#include "Channel.h"
#include "Module.h"

/*
  The Mixer class will be initialized at the beginning of 
  the program. The .begin() method must be called
  after initializing serial communication.

  A module will be connected to a channel by calling the 
  .connectToModule(module, channelIndex) method.
*/
class Mixer
{
  Channel *channels[4];
  public:
    Mixer();
    Channel* getChannel(int index);
    int master = 0;

    void begin();
    int next();

    void connectModuleToChannel(Module module, int channelIndex);

  private:
    void _initializeChannels();
};

#endif