#include "Arduino.h"
#include "Mixer.h"
#include "Channel.h"
#include "Module.h"


Mixer::Mixer()
{

}

void Mixer::begin()
{
  _initializeChannels();
  Serial.println("[init] Mixer initialized");
}

void Mixer::_initializeChannels()
{
  for(int i = 0; i < NUMBER_OF_CHANNELS; i++) {
    channels[i] = new Channel(i);
  }
}

Channel* Mixer::getChannel(int index)
{
  return channels[index];
}

void Mixer::connectModuleToChannel(Module module, int channelIndex) {
  Channel* channel = channels[channelIndex];
  channel->setInput(module);
}

int Mixer::next()
{
  // master = 0;
  return channels[0]->getInput().next();
  // Call each channel's module .next() method to get the next sample
  // for(int i = 0; i < 1; i++) {
    // if(channels[i]->connected == true) {
  //     master += channels[i]->getInput().next();
  //   }
  // }
  // return master;
}