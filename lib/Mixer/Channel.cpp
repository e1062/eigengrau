#include "Arduino.h"
#include "Channel.h"
#include "Module.h"

const int MAX_GAIN = 10;

Channel::Channel(int index)
{
  Serial.print("Initialize channel index: ");
  Serial.println(index);
  _index = index;
}

int Channel::getIndex() 
{
  return _index;
}

void Channel::setInput(Module module) 
{
  _input = module;
  connected = true;
}

Module Channel::getInput()
{
  return _input;
}

void Channel::setGain(int gain)
{ 
  if(gain >= 0 && gain < MAX_GAIN) {
    _gain = gain;
  }

  if(gain > MAX_GAIN) {
    _gain = MAX_GAIN;
  }

  if(gain < 0) {
    _gain = 0;
  }
}