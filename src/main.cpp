// Core Mozzi modules
#include "Arduino.h"
#include "Oscil.h"
#include "EventDelay.h"

// Core Eigengrau modules
#include "Mixer.h"
#include "Orchestrator.h"

// Sound Eigengrau modules
#include "Kick/Kick.h"
#include "SimpleDrone/SimpleDrone.h"


// Initialize core Mozzi modules
EventDelay noteDelay;

// Initialize core modules
Mixer mixer;
Orchestrator orchestrator;

// Initialize sound modules
Kick mKick;
SimpleDrone mSimpleDrone;


void setup() {
  Serial.begin(9600);
  startMozzi(CONTROL_RATE);  
  noteDelay.set(2000);
  
  // Start mixer
  mixer.begin();
  mixer.connectModuleToChannel(mKick, 0);
  mixer.connectModuleToChannel(mSimpleDrone, 1);

  // orchestrator.begin();
  // orchestrator.getCurrentStepState();
  
  Serial.println("[init] Eigengrau Loaded");
}

void updateControl() {
  if(noteDelay.ready()) {
    // Serial.println("Step");
    mKick.step();
    mSimpleDrone.step();
    noteDelay.start(500);
  }
  mSimpleDrone.updateControl();
  mKick.updateControl();
}

AudioOutput_t updateAudio(){
  // int s = mSimpleDrone.next();
  // int t = mKick.next();
  return MonoOutput::fromNBit(9, mixer.next());
}

void loop() {
  // delay(1000);
  // orchestrator.step();
  // orchestrator.getCurrentStepState();
  audioHook();
}